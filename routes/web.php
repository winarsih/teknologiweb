<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['prefix' => 'admin', 'as' => 'admin.'], function () {
    Route::get('/','HomeAdminController@getHomeAdmin');
    Route::get('/wisatautama','HomeAdminController@TampilWisata');
    Route::get('/kategoriutama','HomeAdminController@TampilKategori');

    Route::group(['prefix' => 'kategori', 'as' => 'kategori.'], function () {
        Route::get('/',function() {
            return view('admin/newKategoriAdmin');
        });
        Route::post('/create', 'HomeAdminController@setKategori');
        Route::get('/update/{id}','HomeAdminController@updateKategori');
        Route::post('/edit','HomeAdminController@editKategori');
        Route::get('/delete/{id}','HomeAdminController@deleteKategori');
    });
    Route::group(['prefix' => 'wisata', 'as' => 'wisata.'], function () {
        Route::get('/','HomeAdminController@getKategori');
        Route::post('/create', 'HomeAdminController@setWisata');
        Route::get('/update/{id}','HomeAdminController@updateWisata');
        Route::post('/edit','HomeAdminController@editWisata');
        Route::get('/delete/{id}','HomeAdminController@deleteWisata');
        Route::get('/{id}','HomeAdminController@editPhotoWisata');
        Route::post('/create/gambar','HomeAdminController@setGambar');
        Route::get('/gambar/{id}','HomeAdminController@deleteGambar');
    });

});
Route::group(['prefix' => 'home', 'as' => 'home.'], function () {
    Route::get('/','HomeController@index');
    Route::get('/{id}','HomeController@getWisata');
    Route::get('/kategori/{id}','HomeController@getKategori');
    Route::get('/transaksi/{id}','CheckoutController@getTiket');
    Route::post('/pembayaran','CheckoutController@setTransaksi');
    Route::get('/tiket','CheckoutController@getPayment');
});

Route::get('/Test',function(){
    return view('index');
});



Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');
