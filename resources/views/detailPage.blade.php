@extends('main')
@section('content')
    <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">

        <div class="carousel-inner">
            @foreach($foto as $f)
            <div class="carousel-item {{ $loop->iteration == 1 ? 'active' : null }}">
                <img class="d-block mx-auto " style="height:350px" src="{{ url('/data_file/'.$f->foto) }}">
                <div class="carousel-caption d-none d-md-block">
                    <h5></h5>
                    <p>{{$f->keterangan}}</p>
                </div>
            </div>
            @endforeach
        </div>
        <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
    <br>
    @foreach($wisata as $w)
    <div class="row">
        <div class="col pl-4">
            <h1 class="text-danger"> {{$w->nama}} </h1>
            <h4> {{$w->getTabelKategori->kategori}} </h4>
        </div>
        <div class="col text-right pr-4">
            <h3>{{$w->jam_buka}} - {{$w->jam_tutup}}</h3>
        </div>
    </div>
    <div class=" col text-center">

            <a href="{{ url ('/home/transaksi')}}/{{ $w->id_wisata }}" class="btn btn-success btn-lg pl-5 pr-5">Beli</a>
    </div>

    @endforeach


@endsection
