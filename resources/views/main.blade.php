<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Lets Trip to Malang</title>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,700,900|Display+Playfair:200,300,400,700">
    <link rel="stylesheet" href="{{ url('/fonts/icomoon/style.css')}}">

    <link rel="stylesheet" href="{{ url('/css/home/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{ url('/css/home/magnific-popup.css')}}">
    <link rel="stylesheet" href="{{ url('/css/home/jquery-ui.css')}}">
    <link rel="stylesheet" href="{{ url('/css/home/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{ url('/css/home/owl.theme.default.min.css')}}">
    {{-- <link rel="stylesheet" href="{{ url('/css/home/w3school.css')}}"> --}}
    <link rel="stylesheet" href="{{ url('/css/home/bootstrap-datepicker.css')}}">

    <link rel="stylesheet" href="{{ url('/fonts/flaticon/font/flaticon.css')}}">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/mediaelement@4.2.7/build/mediaelementplayer.min.css">


    <link rel="stylesheet" href="{{ url('/css/home/aos.css')}}">

    <link rel="stylesheet" href="{{ url('/css/home/style.css')}}">
</head>

<body>
    <header class="site-navbar py-1" role="banner">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-6 col-xl-2">
                    <h1 class="mb-0"><a href="index.html" class="text-black h2 mb-0">Malang It!</a></h1>
                </div>
                <div class="col-10 col-md-8 d-none d-xl-block">
                    <nav class="site-navigation position-relative text-right text-lg-center" role="navigation">

                        <ul class="site-menu js-clone-nav mx-auto d-none d-lg-block">
                            <li class="active">
                                <a href="{{ url('/home')}}">Beranda</a>
                            </li>
                            <li class="has-children">
                                <a>Kategori</a>
                                <ul class="dropdown">
                                    @foreach($kategori as $k)
                                    <li><a href="{{ url ('/home/kategori')}}/{{ $k->id_kategori }}">{{$k->kategori}}</a></li>
                                    @endforeach
                                </ul>
                            </li>
                            <li class="ml-1">
                                    <ul class="nav navbar-nav navbar-left ml-auto">
                                            <!-- Authentication Links -->
                                            @guest
                                                <li class="nav-item">
                                                    <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                                                </li>
                                                {{-- @if (Route::has('register'))
                                                    <li class="nav-item">
                                                        <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                                    </li>
                                                @endif --}}
                                            @else
                                                <li class="nav-item dropdown">
                                                    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                                        {{ Auth::user()->name }} <span class="caret"></span>
                                                    </a>

                                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                                        <a class='dropdown-item' href="{{url('/home/tiket')}}">Daftar Tiket</a>
                                                        <a class="dropdown-item" href="{{ route('logout') }}"
                                                           onclick="event.preventDefault();
                                                                         document.getElementById('logout-form').submit();">
                                                            {{ __('Logout') }}
                                                        </a>

                                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                            @csrf
                                                        </form>
                                                    </div>
                                                </li>
                                            @endguest
                                        </ul>

                            </li>

                        </ul>

                    </nav>
                </div>
            </div>
        </div>
    </header>
    @yield('content')

    <script src="{{url('/js/home/jquery-3.3.1.min.js')}}"></script>
    <script src="{{url('/js/home/jquery-migrate-3.0.1.min.js')}}"></script>
    <script src="{{url('/js/home/jquery-ui.js')}}"></script>
    <script src="{{url('/js/home/popper.min.js')}}"></script>
    <script src="{{url('/js/home/bootstrap.min.js')}}"></script>
    <script src="{{url('/js/home/owl.carousel.min.js')}}"></script>
    <script src="{{url('/js/home/jquery.stellar.min.js')}}"></script>
    <script src="{{url('/js/home/jquery.countdown.min.js')}}"></script>
    <script src="{{url('/js/home/jquery.magnific-popup.min.js')}}"></script>
    <script src="{{url('/js/home/bootstrap-datepicker.min.js')}}"></script>
    <script src="{{url('/js/home/aos.js')}}"></script>

    <script src="{{url('/js/home/main.js')}}"></script>
    {{-- <script src="{{url('/js/home/w3school.js')}}"></script> --}}
</body>
</html>
