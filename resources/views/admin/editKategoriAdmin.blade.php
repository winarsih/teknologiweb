@extends('admin.MainAdmin')
@section('judul_halaman', 'Edit Kategori')
@section('content')
<div class="content-container">
  <div class="container-fluid">
  <div class="card pl-5 pr-5 ml-5 mr-5 col-md-8  mx-auto d-block">
    <div class="card-body ">
      <h5 class="card-title"><i class="fa fa-user"></i>Edit Kategori</h5>
      @foreach($kategori as $d)
      <form action="{{ url('admin/kategori/edit')}}" method="POST">
        @csrf

        <input type="text" class="form-control"  name="kategori" id="kategori" placeholder="Nama Kategori" value = "{{$d->kategori}}">
        <input type="text" class="form-control mt-3"  name="keterangan" id="keterangan" placeholder="Keterangan" value = "{{$d->keterangan}}">
        <button type="submit"  class="btn btn-info btn-lg mx-auto d-block mt-3">Submit</button>
      </form>
      @endforeach
    </div>
  </div>
</div>
</div>
  @endsection
