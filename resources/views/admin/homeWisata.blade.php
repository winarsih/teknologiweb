@extends('admin.MainAdmin')
@section('judul_halaman', 'Manage Tempat Wisata')
@section('content')
<div class="content-container">
  <div class="container-fluid">
<div class="card pl-5 pr-5 ml-5 mr-5">
<div class="card-body">
  <h5 class="card-title"><i class="fa fa-user"></i>Tempat Wisata</h5>
  <a href="/admin/wisata" class="btn btn-info mt-3" role="button">Tambah Tempat Wisata</a>
  <table class="table table-striped mt-3">
    <tr>
      <th>Nama Tempat Wisata</th>
      <th>Jam Buka</th>
      <th>Jam Tutup</th>
      <th>Kategori</th>
      <th>Foto</th>
      <th>Harga Tiket</th>
      <th>Aksi</th>
    </tr>
    @foreach($wisata as $idx => $d)
    <tr>
      <td>{{$d->nama}}</td>
      <td>{{$d->jam_buka}}</td>
      <td>{{$d->jam_tutup}}</td>
      <td>{{$d->getTabelKategori->kategori}}</td>
      <td>
        @foreach ($foto[$idx] as $photo)
        @if ($loop->iteration == 1)
        <img width="150px"src="{{ url('/data_file/'.$photo->foto) }}">
        @endif
        @endforeach
      </td>
      <td>{{$d->harga}}</td>
      <td>
        <button  class="btn btn-success" onclick="window.location.href ='{{ url ('/admin/wisata')}}/{{ $d->id_wisata }}';">Foto</button>
        <button class="btn btn-warning"onclick="window.location.href ='{{ url ('/admin/wisata/update')}}/{{ $d->id_wisata }}';">Edit</button>
        <button class="btn btn-danger" onclick="window.location.href ='{{ url ('/admin/wisata/delete')}}/{{ $d->id_wisata }}';">Hapus</button>
      </td>
    </tr>
    @endforeach
  </table>

  <br>
</div>
</div>
</div>
</div>
@endsection
