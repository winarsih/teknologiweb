@extends('admin.MainAdmin')
@section('judul_halaman', 'Manage Tempat Wisata')
@section('content')
<div class="content-container">

  <div class="container-fluid">

    <!-- Main component for a primary marketing message or call to action -->
    <div class="jumbotron">
      <div class="container">
        <div class="row">
        <div class="col-md-3">
          <div class="card-counter primary">
            <i class="fa fa-code-fork"></i>
            <span class="count-numbers">2</span>
            <span class="count-name">Tempat Wisata</span>
          </div>
        </div>

        <div class="col-md-3">
          <div class="card-counter danger">
            <i class="fa fa-ticket"></i>
            <span class="count-numbers">3</span>
            <span class="count-name">Kategori</span>
          </div>
        </div>

        <div class="col-md-3">
          <div class="card-counter success">
            <i class="fa fa-database"></i>
            <span class="count-numbers">2</span>
            <span class="count-name">Transaksi</span>
          </div>
        </div>

        <div class="col-md-3">
          <div class="card-counter info">
            <i class="fa fa-users"></i>
            <span class="count-numbers">2</span>
            <span class="count-name">Users</span>
          </div>
        </div>
      </div>
    </div>
    </div>
  </div>
</div>
@endsection
