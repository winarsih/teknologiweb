@extends('admin.MainAdmin')
@section('judul_halaman', 'Manage Tempat Wisata')
@section('content')
<div class="content-container">
  <div class="container-fluid">
  <div class="card pl-5 pr-5 ml-5 mr-5 col-md-8  mx-auto d-block">
    <div class="card-body ">
      <h5 class="card-title"><i class="fa fa-user"></i>Tambah Tempat Wisata</h5>
      <form action="{{ url('admin/wisata/create')}}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
          <label for="nama">Nama Tempat Wisata</label>
          <input  type="text" name="nama" class="form-control" id="nama" placeholder="Nama Tempat Wisata">
        </div>

        <div class="form-group">
          <label for="nama">Deskripsi</label>
          <textarea name="deskripsi" class="form-control" id="deskripsi" placeholder="Deskripsi" rows="3"></textarea>
        </div>

        <div class="form-group">
          <label for="nama">Alamat</label>
          <input type="text" name="alamat" class="form-control" id="alamat" placeholder="Alamat Tempat Wisata">
        </div>

        <div class="form-group">
          <label for="harga">Harga</label>
        <input type="text" name="harga" class="form-control" id="harga" placeholder="Harga">
        </div>

        <div class="row">
          <div class="col">
            <label for="jam_buka">Jam Buka</label>
            <input type="time" name="jam_buka" id="jam_buka" class="form-control" placeholder="Jam Buka">
          </div>
          <div class="col">
            <label for="jam_tutup">Jam Tutup</label>
            <input type="time" name="jam_tutup" id="jam_tutup" class="form-control" placeholder="Jam Tutup">
          </div>
        </div>

        <div class="input-group mb-3 mt-3">
          <div class="input-group-prepend">
            <label class="input-group-text" for="inputGroupSelect01">Pilih Kategori</label>
          </div>
          <select class="custom-select" name="kategori"id="inputGroupSelect01">
            <option selected>Choose...</option>
            @foreach($kategori as $d)
            <option value='{{$d->id_kategori}}'>{{$d->kategori}}</option>
            @endforeach
          </select>
        </div>
        <br>
        <button type="submit" class="btn btn-info btn-lg mx-auto d-block">Submit</button>
      </form>
    </div>
  </div>
</div>
</div>
  @endsection
