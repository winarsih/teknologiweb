@extends('admin.MainAdmin')
@section('judul_halaman', 'Tambah Gambar Wisata')
@section('content')
<div class="content-container">
  <div class="container-fluid">
  <div class="card pl-5 pr-5 ml-5 mr-5">
    <div class="card-body">
      <h5 class="card-title"><i class="fa fa-user"></i>Detail Tempat Wisata</h5>
      @foreach($wisata as $w)
      <table class="table table-striped mt-3">
        <tr>
          <th>Nama Tempat Wisata</th>
          <th>Jam Buka</th>
          <th>Jam Tutup</th>
          <th>Kategori</th>
        </tr>
        <tr>
          <td>{{$w->nama}}</td>
          <td>{{$w->jam_buka}}</td>
          <td>{{$w->jam_tutup}}</td>
          <td>{{$w->getTabelKategori->kategori}}</td>
        </tr>
      </table><br>

      <form action="/admin/wisata/create/gambar"method="POST" enctype="multipart/form-data">
        {{csrf_field()}}

        <input type="hidden" name="id_wisata" value="{{ $w->id_wisata }}"> <br>
        <div class="form-group">
          <b>File Gambar</b><br/>
          <input type="file"name="file">
        </div>
        <div class="form-group">
          <b>Keterangan</b>
          <textarea class="form-control"name="keterangan"></textarea>
        </div>

          <button type="submit" class="btn btn-info btn-lg mx-auto d-block">Submit</button>
      </form>
      @endforeach
      <br>

      <table class="table table-striped mt-3">
        <thead>
          <tr>
            <th>Gambar</th>
            <th>Keterangan</th>
            <th>Aksi</th>
          </tr>
        </thead>
        <tbody>
          @foreach($foto as$f)
          <tr>
            <td><img width="150px"src="{{ url('/data_file/'.$f->foto) }}"></td>

            <td>{{$f->keterangan}}</td>
            <td><a class="btn btn-danger"href="/admin/wisata/gambar/{{ $f->id_foto }}">HAPUS</a></td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
</div>
</div>
      @endsection
