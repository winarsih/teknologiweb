@extends('admin.MainAdmin')
@section('judul_halaman', 'Manage Tempat Wisata')
@section('content')
<div class="content-container">
  <div class="container-fluid">
  <div class="card pl-5 pr-5 ml-5 mr-5">
    <div class="card-body">
      <h5 class="card-title"><i class="fa fa-user"></i>Kategori</h5>
      <a href="/admin/kategori" class="btn btn-info mt-3" role="button">Tambah Kategori</a>
      <table class="table table-striped mt-3">
        <th>Kategori</th>
        <th>Keterangan</th>
        <th>Aksi</th>
      </tr>
      @foreach($kategori as $d)
      <tr>
        <td>{{$d->kategori}}</td>
        <td>{{$d->keterangan}}</td>
        <td>

          <button  class="btn btn-warning" onclick="window.location.href ='{{ url ('/admin/kategori/update')}}/{{ $d->id_kategori }}';">Edit</button>
          <button  class="btn btn-danger" onclick="window.location.href ='{{ url ('/admin/kategori/delete')}}/{{ $d->id_kategori }}';">Hapus</button>
        </td>

      </tr>
      @endforeach
    </table>

    <br>


  </div>
</div>
</div>
</div>
@endsection
