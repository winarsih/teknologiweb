@extends('admin.MainAdmin')
@section('judul_halaman', 'Edit Tempat Wisata')
@section('content')
<div class="content-container">
  <div class="container-fluid">
  <div class="card pl-5 pr-5 ml-5 mr-5">
    <div class="card-body">
      <h5 class="card-title"><i class="fa fa-user"></i>Edit Tempat Wisata</h5>
      @foreach($wisata as $d)
      <form action="{{ url('admin/wisata/edit')}}" method="POST">
        @csrf
        <input type="hidden" name="id_wisata" value="{{ $d->id_wisata }}"> <br>

        <div class="form-group">
          <label for="nama">Nama Tempat Wisata</label>
          <input  type="text" name="nama" class="form-control" id="nama" placeholder="Nama Tempat Wisata" value="{{ $d->nama}}">
        </div>

        <div class="form-group">
          <label for="harga">Harga</label>
        <input type="text" name="harga" class="form-control" id="harga" placeholder="Harga" value="{{ $d->harga}}">
        </div>

        <div class="form-group">
          <label for="deskripsi">Deskripsi</label>
          <input type="text" name="deskripsi" id="deskripsi" class="form-control" placeholder="Deskripsi" value="{{ $d->deskripsi}}">
        </div>

        <div class="row">
          <div class="col-md-3">
            <input type="time" name="jam_buka" id="jam_buka" class="form-control" placeholder="Jam Buka" value="{{ $d->jam_buka}}">
          </div>
          <div class="col-md-3">
            <input type="time" name="jam_tutup" id="jam_tutup" class="form-control" placeholder="Jam Tutup" value="{{ $d->jam_tutup}}">
          </div>
        </div>

        <div class="form-group mt-3">
          <input type="text" name="alamat" id="alamat" class="form-control" placeholder="Alamat Tempat Wisata" value="{{ $d->alamat}}">
        </div>
          <div class="input-group mb-3 mt-3">
            <div class="input-group-prepend">
              <label class="input-group-text" for="inputGroupSelect01">Pilih Kategori</label>
            </div>
            <select class="custom-select" name="kategori"id="inputGroupSelect01">
              <option selected>Choose...</option>
              @foreach($kategori as $k)
              <option value='{{$k->id_kategori}}'>{{$k->kategori}}</option>
              @endforeach
            </select>
          </div>

            <button type="submit" class="btn btn-info btn-lg mx-auto d-block">Submit</button>
        </form>
        @endforeach
      </div>
    </div>
  </div>
</div>
  @endsection
