@extends('admin.MainAdmin')
@section('judul_halaman', 'Manage Tempat Wisata')
@section('content')
<div class="content-container">
  <div class="container-fluid">
  <div class="card pl-5 pr-5 ml-5 mr-5 col-md-8  mx-auto d-block">
    <div class="card-body ">
      <h5 class="card-title"><i class="fa fa-user"></i>Tambah Kategori</h5>
        <form action="{{ url('admin/kategori/create')}}" method="POST">
            @csrf

            <div class="form-group">
              <label for="kategori">Kategori</label>
              <input type="text" name="kategori" class="form-control"  id="kategori" placeholder="Nama Kategori">
            </div>

            <div class="form-group">
            <label for="keterangan">Keterangan</label>
            <textarea name="keterangan" class="form-control" id="keterangan" placeholder="Keterangan"></textarea>
          </div>

          <button type="submit" class="btn btn-info btn-lg mx-auto d-block">Submit</button>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection
