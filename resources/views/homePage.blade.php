@extends('main')
@section('content')
    {{-- <button onclick="window.location.href ='{{ url('/home')}}';">Home</button>
    <div class="dropdown">
        <button class="dropbtn">Dropdown
            <i class="fa fa-caret-down"></i>
        </button>
        <div class="dropdown-content">
            @foreach($kategori as $k)
            <a href="{{ url ('/home/kategori')}}/{{ $k->id_kategori }}">{{$k->kategori}}</a>
            @endforeach
        </div>
    </div> --}}
    <div class="slide-one-item home-slider owl-carousel">

        <div class="site-blocks-cover overlay" style="background-image: url({{url('/data_file/assets/slider1.jpg')}});" data-aos="fade" data-stellar-background-ratio="0.5">

            <div class="container">
                <div class="row align-items-center justify-content-center text-center">

                    <div class="col-md-8" data-aos="fade-up" data-aos-delay="400">


                        <h1 class="text-white font-weight-light">Never Stop Exploring</h1>
                        <p class="mb-5">Invite your family to have vacation</p>
                        <p><a href="#" class="btn btn-primary py-3 px-5 text-white">Book Now!</a></p>

                    </div>
                </div>
            </div>
        </div>
        <div class="site-blocks-cover overlay" style="background-image: url({{url('/data_file/assets/slider2.jpg')}});" data-aos="fade" data-stellar-background-ratio="0.5">
            <div class="container">
                <div class="row align-items-center justify-content-center text-center">

                    <div class="col-md-8" data-aos="fade-up" data-aos-delay="400">
                        <h1 class="text-white font-weight-light">Love The Places</h1>
                        <p class="mb-5">Beautiful place for lovely person</p>
                        <p><a href="#" class="btn btn-primary py-3 px-5 text-white">Book Now!</a></p>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <div class="site-section">

        <div class="container overlap-section">
            <div class="row">
                @foreach($wisata as $idx => $d)
                <div class="col-md-6 col-lg-4 mb-4 mb-lg-0 mt-3">
                    <a href="{{ url ('/home')}}/{{ $d->id_wisata }}" class="unit-1 text-center">
                        @foreach ($foto[$idx] as $photo)
                            @if ($loop->iteration == 1)
                            <img src="{{ url('/data_file/'.$photo->foto) }}" alt="Image" class="img-fluid">
                            @endif
                        @endforeach
                        <div class="unit-1-text">
                            <h3 class="unit-1-heading">{{$d->nama}}</h3>
                            <h5 class="unit-1-heading">{{$d->jam_buka}} - {{$d->jam_tutup}}</h5>
                            <h5 class="unit-1-heading">{{$d->getTabelKategori->kategori}}</h5>
                        </div>
                    </a>
                </div>
                @endforeach
            </div>
        </div>

    </div>

        {{-- <table border=1>
                <tr>
                    <th>Nama Tempat Wisata</th>
                    <th>Operasional</th>
                    <th>Foto</th>
                    <th>Detail</th>
                </tr>
                @foreach($wisata as $idx => $d)
                <tr>
                    <td>{{$d->nama}}</td>
                    <td>{{$d->jam_buka}} - {{$d->jam_tutup}}</td>
                    <td>
                        @foreach ($foto[$idx] as $photo)
                            @if ($loop->iteration == 1)
                            <img width="150px"src="{{ url('/data_file/'.$photo->foto) }}">
                            @endif
                        @endforeach
                    </td>
                    <td><button onclick="window.location.href ='{{ url ('/home')}}/{{ $d->id_wisata }}';">Detail</button></td>
                </tr>
                @endforeach
            </table><br> --}}

@endsection
