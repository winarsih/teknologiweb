<!DOCTYPE html>
<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
        integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
        integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous">
    </script>
    <style>
        body {
            font-family: Arial;
            font-size: 17px;
            padding: 8px;
        }

        * {
            box-sizing: border-box;
        }

        .row {
            display: -ms-flexbox;
            /* IE10 */
            display: flex;
            -ms-flex-wrap: wrap;
            /* IE10 */
            flex-wrap: wrap;
            margin: 0 -16px;
        }

        .col-25 {
            -ms-flex: 25%;
            /* IE10 */
            flex: 18%;
        }

        .col-50 {
            -ms-flex: 50%;
            /* IE10 */
            flex: 50%;
        }

        .col-75 {
            -ms-flex: 75%;
            /* IE10 */
            flex: 75%;
        }

        .col-25,
        .col-50,
        .col-75 {
            padding: 0px;
        }

        .container {
            background-color: #f2f2f2;
            padding: 5px 20px 15px 20px;
            border: 1px solid lightgrey;
            border-radius: 3px;
        }

        input[type=text] {
            width: 100%;
            margin-bottom: 20px;
            padding: 12px;
            border: 1px solid #ccc;
            border-radius: 3px;
        }

        label {
            margin-bottom: 10px;
            display: block;
        }

        .icon-container {
            margin-bottom: 20px;
            padding: 7px 0;
            font-size: 24px;
        }

        a {
            color: #2196F3;
        }

        hr {
            border: 1px solid lightgrey;
        }

        img {
            width: 800px;
        }

        .img {
            width: 200px;
            height: 100px;
            margin-bottom: 20px;
        }

        span.price {
            float: right;
            color: grey;
        }

        /* Responsive layout - when the screen is less than 800px wide, make the two columns stack on top of each other instead of next to each other (also change the direction - make the "cart" column go on top) */
        @media (max-width: 800px) {
            .row {
                flex-direction: column-reverse;
            }

            .col-25 {
                margin-bottom: 20px;
            }
        }
    </style>
</head>

<body>
    <div class="row">
        @foreach($wisata as $idx => $w)
        <div class="container">
            <h3 class="text-left">{{$w->nama}}</h3>
            <i class="fa fa-map-marker" style="font-size:15px;color:grey;">&nbsp Malang, Jawa Timur</i>
            <hr>
            <div class="row">
                <div class="col-75">
                    <div class="text-center">
                        @foreach ($foto[$idx] as $photo)
                            @if ($loop->iteration == 1)
                            <img src="{{ url('/data_file/'.$photo->foto) }}" class="img-fluid" alt="Responsive image">
                            @endif
                        @endforeach

                    </div>
                </div>
                {{-- <div class="col-25">
                    <img src="{{ asset('/image/image2.jpg')}}" class="img rounded" alt="Responsive image">
                    <img src="{{ asset('/image/image3.jpg')}}" class="img rounded" alt="Responsive image">
                    <img src="{{ asset('/image/image4.jpg')}}" class="img rounded" alt="Responsive image">
                    <img src="{{ asset('/image/image4.jpg')}}" class="img rounded" alt="Responsive image">
                </div> --}}
            </div>
        </div>

        <div class="container">
            <h3>Tiket Masuk {{$w->nama}}</h3>
            <h4>Rp. {{$w->harga}}</h4>
            <form action="{{ url('home/pembayaran')}}" method="POST">
                @csrf
                <input type="hidden" name="id_wisata" value="{{ $w->id_wisata }}"> <br>
                <input type="hidden" name="id_user" value="{{ Auth::user()->id }} "> <br>
                <input type="hidden" name="harga" value="{{ $w->harga }}"> <br>
                <div class="row">
                    <label style="font-size: 13px;" for="example-date-input" class="col-2 col-form-label text-center">Pilih
                        Tanggal Kunjungan</label>
                    <div class="col-3">
                        <input class="form-control" name="tgl_tiket" type="date" value="2011-08-19" id="example-date-input">
                    </div>
                    <label style="font-size: 13px;" class="col-2 col-form-label text-center"
                        for="inlineFormCustomSelect">Pilih Jumlah Pengunjung</label>
                    <div class="col-3">
                        <input class="form-control" type="number" name="jumlah_tiket" id="jumlah_tiket">
                    </div>
                    <div class="col-2">
                        <button class="btn btn-primary" type="submit">Pesan Sekarang</button>
                    </div>
                </div>
            </form>
        </div>
        @endforeach
    </div>

</body>

</html>
