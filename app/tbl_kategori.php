<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tbl_kategori extends Model
{
    protected $table = "tbl_kategori";
    protected $fillable = ['kategori','keterangan'];


    public function getTabelWisata()
    {
        return $this->hasMany('App\tbl_wisata');
    }
}
