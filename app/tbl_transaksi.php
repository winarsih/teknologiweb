<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tbl_transaksi extends Model
{
    protected $table = "tbl_transaksi";
    protected $fillable = ['id_wisata','id_user','jumlah_tiket','tgl_order','tgl_tiket','total_harga'];
    public $timestamps = false;
}
