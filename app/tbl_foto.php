<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tbl_foto extends Model
{
    protected $table = "tbl_foto";
    protected $fillable = ['id_wisata','foto','keterangan'];
}
