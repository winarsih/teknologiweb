<?php

namespace App\Http\Controllers;

use App\tbl_foto;
use App\tbl_wisata;
use App\tbl_kategori;
use Illuminate\Http\Request;
use DB;
use View;
use Illuminate\Support\Carbon;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $kategori = tbl_kategori::all();
        View::share('kategori', $kategori);
        //$this->middleware('auth');

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $now = Carbon::now();
        $month = $now->month;
        if ($month==12||$month==5||$month==1||$month==6||$month==7){
            $wisata = tbl_wisata::whereIn('kategori', [1,2])->orderBy('kategori', 'ASC')->get();
        }else $wisata = tbl_wisata::whereIn('kategori', [1,2])->orderBy('kategori', 'DESC')->get();
        $kategori = tbl_kategori::all();
        $foto = array();

        foreach ($wisata as $idx => $data) {
            $result = tbl_foto::where('id_wisata', $data->id_wisata)->get();
            array_push($foto, $result);
        }

        return view('homePage',['wisata'=>$wisata, 'foto'=>$foto]);
        //return view('homePage');
    }
    public function getWisata($id){
        $wisata = tbl_wisata::where('id_wisata',$id)->get();
        $foto = tbl_foto::where('id_wisata',$id)->get();
        return view('detailPage',['wisata'=>$wisata, 'foto'=>$foto]);
    }
    public function getKategori($id){

        $wisata = tbl_wisata::where('kategori',$id)->get();
        $foto = array();

        foreach ($wisata as $idx => $data) {
            $result = tbl_foto::where('id_wisata', $data->id_wisata)->get();
            array_push($foto, $result);
        }
        $kategori = tbl_kategori::all();
        return view('homePage',['wisata'=>$wisata, 'foto'=>$foto, 'kategori'=>$kategori]);

    }

}
