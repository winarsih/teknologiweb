<?php

namespace App\Http\Controllers;

use App\tbl_wisata;
use App\tbl_kategori;
use App\tbl_foto;
use App\tbl_transaksi;
use App\tbl_payment;
use View;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class CheckoutController extends Controller
{
    public function __construct()
    {
        $kategori = tbl_kategori::all();
        View::share('kategori', $kategori);
        $this->middleware('auth');

    }
    public function getTiket($id){
        $wisata = tbl_wisata::where('id_wisata',$id)->get();
        $foto = array();

        foreach ($wisata as $idx => $data) {
            $result = tbl_foto::where('id_wisata', $data->id_wisata)->get();
            array_push($foto, $result);
        }
        return view('transaksiPage',['wisata' => $wisata,'foto' => $foto]);
    }
    public function setTransaksi(Request $request){
        $setTransaksi = new tbl_transaksi;
        $setTransaksi->id_wisata = $request->id_wisata;
        $setTransaksi->id_user = $request->id_user;
        $setTransaksi->jumlah_tiket = $request->jumlah_tiket;
        $setTransaksi->tgl_tiket = $request->tgl_tiket;
        $setTransaksi->tgl_order = Carbon::now();
        $setTransaksi->total_harga = $request->harga * $request->jumlah_tiket;
        $setTransaksi->save();
        $setPayment = new tbl_payment;
        $setPayment->id_transaksi = tbl_transaksi::max('id_transaksi');
        $setPayment->foto_bukti = "kosong";
        $setPayment->status = false;
        $setPayment->save();
        return redirect()->back();
    }
    public function getPayment(){

        $payment  = tbl_transaksi::where('id_user',Auth::user()->id)->get();
        return view('tiketPage',['payment',$payment]);

    }
}
