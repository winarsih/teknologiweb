<?php

namespace App\Http\Controllers;

use App\tbl_kategori;
use App\tbl_wisata;
use App\tbl_foto;
use File;

use Illuminate\Http\Request;

class HomeAdminController extends Controller
{
    public function getHomeAdmin()
    {
        $wisata = tbl_wisata::all();
        $kategori = tbl_kategori::all();

        $foto = array();

        foreach ($wisata as $idx => $data) {
            $result = tbl_foto::where('id_wisata', $data->id_wisata)->get();
            array_push($foto, $result);
        }

        return view('admin\homeAdmin', ['wisata' => $wisata, 'kategori' => $kategori, 'foto' => $foto]);
    }

    public function TampilKategori()
    {
        $wisata = tbl_wisata::all();
        $kategori = tbl_kategori::all();

        $foto = array();

        foreach ($wisata as $idx => $data) {
            $result = tbl_foto::where('id_wisata', $data->id_wisata)->get();
            array_push($foto, $result);
        }

        return view('admin\homeKategori', ['wisata' => $wisata, 'kategori' => $kategori, 'foto' => $foto]);
    }

    public function TampilWisata()
    {
        $wisata = tbl_wisata::all();
        $kategori = tbl_kategori::all();

        $foto = array();

        foreach ($wisata as $idx => $data) {
            $result = tbl_foto::where('id_wisata', $data->id_wisata)->get();
            array_push($foto, $result);
        }

        return view('admin\homeWisata', ['wisata' => $wisata, 'kategori' => $kategori, 'foto' => $foto]);
    }


    public function setKategori(Request $request)
    {
        $setKategori = $request->all();
        tbl_kategori::create($setKategori);
        return redirect()->to('/admin');
    }
    public function getKategori()
    {
        $kategori = tbl_kategori::select('id_kategori', 'kategori')->get();
        return view('admin\newWisataAdmin', ['kategori' => $kategori]);
    }
    public function updateKategori($id)
    {
        $kategori = tbl_kategori::where('id_kategori', $id)->get();
        return view('admin\editKategoriAdmin', ['kategori' => $kategori]);
    }
    public function editKategori(Request $request)
    {
        $updateKategori = $request->except(['_token']);
        tbl_kategori::where('id_kategori', $request->id_kategori)->update($updateKategori);
        return redirect()->to('/admin');
    }
    public function deleteKategori($id)
    {
        tbl_kategori::where('id_kategori', $id)->delete();
        return redirect()->to('/admin');
    }
    public function setWisata(Request $request)
    {
        // $setWisata = new tbl_wisata;
        // $setWisata->nama = $request->nama;
        // $setWisata->deskripsi = $request->deskripsi;
        // $setWisata->jam_buka = $request->jam_buka;
        // $setWisata->jam_tutup = $request->jam_tutup;
        // $setWisata->alamat = $request->alamat;
        // $setWisata->kategori = $request->kategori;
        // $setWisata->save();
        $setWisata = $request->all();
        tbl_wisata::create($setWisata);
        $gambar = tbl_wisata::max('id_wisata');
        //return view('newGambarWisataAdmin',['gambar'=>$gambar]);
        return redirect()->to('/admin');
    }
    public function updateWisata($id)
    {
        $wisata = tbl_wisata::where('id_wisata', $id)->get();
        $kategori = tbl_kategori::select('id_kategori', 'kategori')->get();
        return view('admin\editWisataAdmin', ['wisata' => $wisata, 'kategori' => $kategori]);
    }
    public function editWisata(Request $request)
    {
        $updateWisata = $request->except(['_token']);
        tbl_wisata::where('id_wisata', $request->id_wisata)->update($updateWisata);
        return redirect()->to('/admin');
    }
    public function deleteWisata($id)
    {
        tbl_wisata::where('id_wisata', $id)->delete();
        return redirect()->to('/admin');
    }
    public function editPhotoWisata($id)
    {
        $wisata = tbl_wisata::where('id_wisata',$id)->get();
        $foto = tbl_foto::where('id_wisata', $id)->get();
        return view('admin\newGambarWisataAdmin', ['foto' => $foto, 'wisata' => $wisata]);
    }
    public function setGambar(Request $request)
    {
        $file = $request->file('file');
        $nama_file = time() . "_" . $file->getClientOriginalName();
        $tujuan_upload = 'data_file';
        $file->move($tujuan_upload, $nama_file);
        tbl_foto::create(['id_wisata' => $request->id_wisata, 'foto' => $nama_file, 'keterangan' => $request->keterangan,]);
        return redirect()->back()   ;
    }
    public function deleteGambar($id)
    {
        $gambar = tbl_foto::where('id_foto', $id)->first();
        File::delete('data_file/' . $gambar->file); // hapus data
        tbl_foto::where('id_foto',$id)->delete();
        return redirect()->back();
    }
}
