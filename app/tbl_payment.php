<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tbl_payment extends Model
{
    protected $table = "tbl_payment";
    protected $fillable = ['id_transaksi','foto_bukti','status'];
    public $timestamps = false;

    public function getTabelTransaksi()
    {
        return $this->belongsTo('App\tbl_transaksi','id_transaksi','id_transaksi');
    }

}
