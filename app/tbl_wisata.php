<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class tbl_wisata extends Model
{
    protected $table = "tbl_wisata";
    protected $fillable = ['nama','deskripsi','jam_buka','jam_tutup','alamat','kategori','harga'];

    public function getTabelKategori()
    {
        return $this->belongsTo('App\tbl_kategori','kategori','id_kategori');
    }

}
