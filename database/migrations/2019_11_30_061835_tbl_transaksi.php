<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TblTransaksi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_transaksi', function (Blueprint $table){
            $table->increments('id_transaksi')->autoIncrement();
            // $table->unsignedInteger('id_tiket');
            $table->unsignedInteger('id_wisata');
            $table->unsignedBigInteger('id_user');
            $table->integer('jumlah_tiket');
            $table->dateTime('tgl_order');
            $table->dateTime('tgl_tiket');
            $table->float('total_harga');
            // $table->foreign('id_tiket')->references('id_tiket')
            // ->on('tbl_tiket')->onDelete('cascade');
            $table->foreign('id_wisata')->references('id_wisata')
            ->on('tbl_wisata')->onDelete('cascade');
            $table->foreign('id_user')->references('id')
            ->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_transaksi');
    }
}
