<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TblPayment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_payment',function (Blueprint $table){
            $table->increments('id_payment')->autoIncrement();
            $table->unsignedInteger('id_transaksi');
            $table->string('foto_bukti',255);
            $table->binary('status');
            $table->timestamps();
            $table->foreign('id_transaksi')->references('id_transaksi')
            ->on('tbl_transaksi')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_payment');
    }
}
