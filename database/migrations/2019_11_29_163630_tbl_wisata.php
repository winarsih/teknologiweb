<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TblWisata extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_wisata',function (Blueprint $table){
            $table->increments('id_wisata')->autoIncrement();
            $table->string('nama',255);
            $table->string('deskripsi',255);
            $table->time('jam_buka');
            $table->time('jam_tutup');
            $table->string('alamat',255);
            $table->bigInteger('harga');
            $table->unsignedInteger('kategori');
            $table->foreign('kategori')
            ->references('id_kategori')
            ->on('tbl_kategori')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_wisata');
    }
}
