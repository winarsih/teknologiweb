<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TblFoto extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_foto',function(Blueprint $table){
            $table->increments('id_foto');
            $table->unsignedInteger('id_wisata');
            $table->string('foto',255);
            $table->string('keterangan',255);
            $table->foreign('id_wisata')->references('id_wisata')->on('tbl_wisata')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_foto');
    }
}
