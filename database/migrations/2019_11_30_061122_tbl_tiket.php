<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TblTiket extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_tiket', function (Blueprint $table){
            $table->increments('id_tiket')->autoIncrement();
            $table->unsignedInteger('id_wisata');
            $table->float('harga');
            $table->timestamps();
            $table->foreign('id_wisata')->references('id_wisata')
            ->on('tbl_wisata')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_tiket');
    }
}
